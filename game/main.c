// pw_gb
#include "pw_gb/pw_gb.c"
// Resources
#include "tiles/tiles.c"
// Entities
#include "objects/chicken.c"
#include "objects/platform.c"
#include "objects/hud.c"
// Screens
#include "screen/screens.c"
#include "screen/screen_game.c"
#include "screen/screen_splash.c"


//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
GameInit()
{
    disable_interrupts();
    DISPLAY_OFF;

    SPRITES_8x8;

    SHOW_BKG;
    SHOW_SPRITES;

    DISPLAY_ON;
    enable_interrupts();
}


//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
main()
{
    Random_Init(1);
    // Screen_Change(SCREEN_TYPE_GAME);
    Screen_Change(SCREEN_TYPE_SPLASH);

    while(1) {
        Screen_Update();
    }
}
