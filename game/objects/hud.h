#ifndef __SCORE_H__
#define __SCORE_H__


//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
void Hud_UpdateScore();

void Hud_ResetDoubleJump ();
void Hud_AddDoubleJump   ();
void Hud_RemoveDoubleJump();

#endif // __SCORE_H__
