// Header
#include "platform.h"
// Game
#include "objects/chicken.h"
#include "objects/hud.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define PLATFORM_SLOT_FIRST 4
#define PLATFORM_SLOT_END   6
#define PLATFORM_SLOT_COUNT 3

#define PLATFORM_SPRITE_INDEX_LEFT   29
#define PLATFORM_SPRITE_INDEX_MIDDLE 30
#define PLATFORM_SPRITE_INDEX_RIGHT  31

#define PLATFORM_SPEED_TO_FALL 2


//----------------------------------------------------------------------------//
// Helpers Functions                                                          //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
_Platform_Randomize(I8 curr_index, I8 left_index)
{
    Platforms[curr_index].hits = 0;
    Platforms[curr_index].shake = FALSE;
    Platforms[curr_index].y     = Random_U8(MIN_PLATFORM_GAP_Y, MAX_PLATFORM_GAP_Y);

    // printf("%d - %d\n", curr_index, Platforms[curr_index].y);
    // First platform is ALWAYS bellow the chicken.
    if(curr_index == 0 && left_index == -1) {
        Platforms[curr_index].x        = CHICKEN_X;
        Platforms[curr_index].max_hits = 0;
    } else {
        Platforms[curr_index].x        = Platforms[left_index].x + TILE_SIZE_x3 + Random_U8(MIN_PLATFORM_GAP_X, MAX_PLATFORM_GAP_X);
        Platforms[curr_index].max_hits = Random_U8(1, 3);
    }

    // printf("%d - %d %d\n", curr_index, Platforms[curr_index].x, Platforms[curr_index].y);
}

//------------------------------------------------------------------------------
void
_Platform_MoveSprite(U8 slot, U8 x, U8 y)
{
    if((x >= LAST_PIXEL_X) || (y + TILE_SIZE < FIRST_PIXEL_Y || y >= LAST_PIXEL_Y))  {
        x = 0;
        y = 0;
    }

    move_sprite(slot, x, y);
}


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Platform_ResetAll()
{
    U8 index;
    U8 offset;

    index  = 0;
    offset = 0;
    for(index = 0; index < PLATFORMS_COUNT; ++index) {
        _Platform_Randomize(index, index-1);

        offset = (index * PLATFORM_SLOT_COUNT);

        // Set the sprites.
        set_sprite_tile(PLATFORM_SLOT_FIRST     + offset, PLATFORM_SPRITE_INDEX_LEFT  );
        set_sprite_tile(PLATFORM_SLOT_FIRST + 1 + offset, PLATFORM_SPRITE_INDEX_MIDDLE);
        set_sprite_tile(PLATFORM_SLOT_FIRST + 2 + offset, PLATFORM_SPRITE_INDEX_RIGHT );

        // @DEBUG(stdmatt): Shows the INDEX.
        // set_sprite_tile(PLATFORM_SLOT_FIRST + 0 + offset, 32 + index);
        // set_sprite_tile(PLATFORM_SLOT_FIRST + 1 + offset, 32 + index);
        // set_sprite_tile(PLATFORM_SLOT_FIRST + 2 + offset, 32 + index);

        // Move the sprites.
        _Platform_MoveSprite(PLATFORM_SLOT_FIRST     + offset, Platforms[index].x,                Platforms[index].y);
        _Platform_MoveSprite(PLATFORM_SLOT_FIRST + 1 + offset, Platforms[index].x + TILE_SIZE,    Platforms[index].y);
        _Platform_MoveSprite(PLATFORM_SLOT_FIRST + 2 + offset, Platforms[index].x + TILE_SIZE_x2, Platforms[index].y);
    }
}

//------------------------------------------------------------------------------
void
Platform_Update()
{
    U8 index;
    U8 offset;
    I16 x;
    I16 y;

    index  = 0;
    offset = 0;

    if(Shake_IsShaking) {
        Shake_Update();
    }

    for(index = 0; index < PLATFORMS_COUNT; ++index)  {
        Platforms[index].x += PlatformSpeedX;

        // Out of the left bounds - Reset it...
        if(Platforms[index].x + TILE_SIZE_x3 <= FIRST_PIXEL_X) {
            if(index == 0) {
                _Platform_Randomize(0, PLATFORMS_COUNT -1);
            } else {
                _Platform_Randomize(index, index -1);
            }

            Chicken_IncrementScore();
        }

        // Falling...
        if(Platforms[index].max_hits != 0 && Platforms[index].hits >= Platforms[index].max_hits) {
            Platforms[index].y += PLATFORM_SPEED_TO_FALL;
            if(Platforms[index].y >= LAST_PIXEL_Y) {
                Platforms[index].y        = 0; // "Hide" de platform
                Platforms[index].max_hits = 0; // Make it stop falling.
            }
        }
        x = Platforms[index].x;
        y = Platforms[index].y;

        // Shake...
        if(Platforms[index].shake) {
            x += Shake_x;
            y += Shake_y;
        }

        // Update the sprites.
        offset = (index * PLATFORM_SLOT_COUNT);


        _Platform_MoveSprite(PLATFORM_SLOT_FIRST     + offset, x,                y);
        _Platform_MoveSprite(PLATFORM_SLOT_FIRST + 1 + offset, x + TILE_SIZE,    y);
        _Platform_MoveSprite(PLATFORM_SLOT_FIRST + 2 + offset, x + TILE_SIZE_x2, y);
    }
}
