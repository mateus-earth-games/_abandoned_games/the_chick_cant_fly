// Header
#include "screen_game.h"
// Game
#include "game_defs.h"

#include "objects/chicken.h"
#include "objects/platform.h"
#include "objects/hud.h"


//----------------------------------------------------------------------------//
// Vars                                                                       //
//----------------------------------------------------------------------------//
// Externals Definitions
Chicken_t  Chicken;
Platform_t Platforms[PLATFORMS_COUNT];

I8   PlatformSpeedX;
BOOL SuperGravity;
BOOL DoubleJump;


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Game_Init()
{
    VBK_REG = 1;
    HIDE_SPRITES;
    HIDE_BKG;

    // Load the sprites.
    set_sprite_data(0, OBJECTS_TILES_COUNT, TILES_OBJECTS);

    // Load the background.
    set_bkg_data(0, 128, TILES_BACKGROUND);
    set_bkg_tiles(0, 0, 40, 18, MAP_BACKGROUND);
    move_bkg(0, 0);

    // Make sure that we are in the Fade In phase.
    Fade_Reset(FADE_FRAMES_COUNT, FADE_IN);
    SHOW_BKG;
    VBK_REG = 0;
}

//------------------------------------------------------------------------------
void
Game_Update()
{
    U8 _delay;

    Input_Reset();

    _delay = 1;
    while(1) {
        //
        // Fading...
        if(!Fade_IsCompleted) {
            Fade_Update();
            if(Fade_State == FADE_IN && Fade_IsCompleted) {
                // Reset game state.
                PlatformSpeedX = 0;
                SuperGravity   = FALSE;
                DoubleJump     = FALSE;

                Platform_ResetAll();
                Chicken_Reset    ();

                Hud_UpdateScore();
                Hud_ResetDoubleJump();

                Delay(1);
                SHOW_SPRITES;
            } else if(Fade_State == FADE_OUT && Fade_IsCompleted) {
                Fade_Reset(FADE_FRAMES_COUNT, FADE_IN);
            }
        }
        //
        // Playing...
        else {
            PlatformSpeedX = -0;
            Input_Curr = joypad();

            // Input
            if(JOY_PRESS(J_LEFT)) {
                PlatformSpeedX += -3;
            }

            if(JOY_PRESS(J_A)) {
                SuperGravity = TRUE;
            } else {
                SuperGravity = FALSE;
            }


            if(JOY_CLICK(J_B)) {
                DoubleJump = TRUE;
            } else {
                DoubleJump = FALSE;
            }

            if(Chicken.state == CHICKEN_STATE_DYING || Chicken.state == CHICKEN_STATE_DEAD) {
                PlatformSpeedX = 0;
                SuperGravity   = FALSE;
                DoubleJump     = FALSE;
            }

            if(Chicken.state == CHICKEN_STATE_DEAD) {
                Fade_Reset(FADE_FRAMES_COUNT, FADE_OUT);
                HIDE_SPRITES;
            }

            Chicken_Update ();
            Platform_Update();

            if(PlatformSpeedX) {
                scroll_bkg(-(PlatformSpeedX) -1,  0);
            }

            //
            // @DEBUG(stdmatt): Regulate the speed of the game so we can debug...
            // if(JOY_CLICK(J_UP)) {
            //     _delay += 1;
            // } else if(JOY_CLICK(J_DOWN)) {
            //     if(_delay <= 0) {
            //         _delay = 0;
            //     }
            //     _delay -= 1;
            // }

            Input_Prev = Input_Curr;
        }

        // Refresh...
        Delay(_delay);
        wait_vbl_done();
    }
}

//------------------------------------------------------------------------------
void
Game_Quit()
{
    // Empty...
}
