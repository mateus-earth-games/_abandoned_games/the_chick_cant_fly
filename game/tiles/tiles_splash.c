/*

 TILES_SPLASH.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 127

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char TILES_SPLASH[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x1F,0x1F,0x3F,0x37,
  0x7F,0x27,0x7F,0x2F,0x7F,0x3F,0x7F,0x3F,
  0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0xF8,0xF8,0xFD,0xFD,
  0xFF,0xFD,0xFF,0xFD,0xFF,0xFD,0xFF,0xFD,
  0x00,0x00,0x00,0x00,0xFE,0xFE,0xFE,0xBE,
  0xFE,0x3E,0xFE,0x7E,0xFE,0xFE,0xFE,0xFE,
  0x00,0x00,0x00,0x00,0x3F,0x3F,0x7F,0x27,
  0x7F,0x2F,0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,
  0x00,0x00,0x00,0x00,0x9F,0x9F,0xFF,0xDB,
  0xFF,0xD7,0xFF,0xD7,0xFF,0xDF,0xFF,0xDF,
  0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x80,
  0x80,0x80,0x81,0x80,0x81,0x81,0x83,0x83,
  0x00,0x00,0x00,0x00,0x0F,0x0F,0x3F,0x3B,
  0xFF,0xE3,0xFF,0xCF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0xE1,0xE1,0xE3,0xE3,
  0xF7,0xF2,0xF7,0xF2,0xF7,0xF3,0xF7,0xF3,
  0x00,0x00,0x00,0x00,0xFC,0xFC,0xFC,0x3C,
  0xFC,0x7C,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,
  0x00,0x00,0x00,0x00,0x7F,0x7F,0xFF,0x4F,
  0xFF,0x5F,0xFF,0x5F,0xFF,0x7F,0xFF,0x7F,
  0x00,0x00,0x00,0x00,0x1F,0x1F,0xBF,0xB3,
  0xFF,0xA7,0xFF,0xAF,0xFF,0xBF,0xFF,0xBF,
  0x00,0x00,0x00,0x00,0xE0,0xE0,0xE0,0xE0,
  0xF0,0xF0,0xF1,0xF0,0xF1,0xF1,0xF3,0xF3,
  0x00,0x00,0x00,0x00,0x0F,0x0F,0x3F,0x33,
  0xFF,0xE7,0xFF,0xCF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0xE1,0xE1,0xE3,0xE3,
  0xF7,0xF2,0xF7,0xF2,0xF7,0xF3,0xF7,0xF3,
  0x00,0x00,0x00,0x00,0xFC,0xFC,0xFE,0x3E,
  0xFE,0x7E,0xFF,0x7F,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0x00,0x00,0x78,0x78,
  0xFC,0xCC,0xFE,0x9E,0xFE,0xFE,0xFE,0xFE,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x3F,0x7F,0x1F,0x3F,0x00,0x01,0x00,
  0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFD,0xFB,0xF9,0xF3,0x01,0x03,0x01,
  0x03,0x01,0x03,0x00,0x01,0x00,0x01,0x00,
  0xFE,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x7F,0x3F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xDF,0xFF,0xDF,0xFF,0xDF,0xFF,0xDF,
  0xFF,0xDF,0xFF,0xDF,0xFF,0xDF,0xFF,0xDF,
  0xFF,0xE0,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,
  0xFC,0xFC,0xFC,0xFC,0xF8,0xE0,0xFF,0xFF,
  0x07,0x03,0x07,0x07,0x0F,0x07,0x0F,0x07,
  0x0F,0x07,0x0F,0x07,0x0F,0x07,0xCF,0xC7,
  0xFF,0xFF,0xFF,0xFE,0xFC,0xFC,0xF8,0xF8,
  0xF8,0xF8,0xF8,0xF8,0xFF,0xFF,0xFF,0xFF,
  0xE7,0xE3,0xC7,0x03,0x07,0x03,0x07,0x03,
  0x07,0x03,0x07,0x01,0xF3,0xF1,0xF3,0xF1,
  0xFC,0xFC,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x9F,0xBF,0x9F,0xBF,0x9F,0xBF,0x9F,
  0xBF,0x9F,0xBF,0x9F,0xBF,0x9F,0xBF,0x9F,
  0xF7,0xF3,0xF7,0xF7,0xFF,0xF7,0xFF,0xF7,
  0xFF,0xF7,0xFF,0xF7,0xFF,0xF7,0xEF,0xE7,
  0xFF,0xFF,0xFF,0xFE,0xFC,0xFC,0xF8,0xF8,
  0xF8,0xF8,0xF8,0xF8,0xFF,0xFF,0xFF,0xFF,
  0xE7,0xE3,0xC7,0x03,0x07,0x03,0x07,0x03,
  0x07,0x03,0x07,0x03,0xF7,0xF3,0xF7,0xF3,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFC,0xFC,0xFC,0xFC,0xF8,0xF8,0xF0,0xF0,
  0xF0,0xF0,0xF8,0xF8,0xFE,0xFE,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
  0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x7F,0xFE,0x7E,0xFC,0x00,0x0F,0x0F,
  0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
  0x01,0x00,0x00,0x00,0x00,0x00,0xE0,0xE0,
  0xFF,0xFF,0xFF,0xFE,0xFE,0xFE,0xFE,0xFE,
  0xFE,0x7E,0xFE,0x7E,0xFC,0x00,0x0F,0x0F,
  0xFF,0xFF,0xFF,0x3F,0x7F,0x3F,0x7F,0x1F,
  0x3F,0x1F,0x3F,0x1F,0x3F,0x00,0xE0,0xE0,
  0xFF,0xDF,0xFF,0xDF,0xBF,0x9F,0xBF,0x8F,
  0x9F,0x8F,0x9F,0x8F,0x1F,0x00,0x1F,0x1F,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFE,0x00,0xC3,0xC3,
  0xCF,0xC3,0xC7,0xC3,0xC7,0xC1,0xC3,0xC0,
  0xC1,0xC0,0x80,0x00,0x00,0x00,0xF9,0xF9,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x7F,0xFF,0x0F,0x1F,0x00,0xE7,0xE7,
  0xFB,0xF9,0xFB,0xF9,0xFB,0xF9,0xFB,0xF9,
  0xFB,0xF8,0xF1,0xE0,0xC1,0x00,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFC,0xFC,0xFC,0xFC,0xFC,
  0xFC,0xFC,0xFC,0xFC,0xF8,0x00,0xFE,0xFE,
  0xFF,0xFF,0xFF,0x7F,0xFF,0x7F,0xFF,0x3F,
  0x7F,0x3F,0x7F,0x3F,0x7E,0x00,0x3F,0x3F,
  0xBF,0x9F,0xBF,0x9F,0x3F,0x1F,0x3F,0x0F,
  0x1F,0x0F,0x1F,0x0F,0x1F,0x00,0xFF,0xFF,
  0xEF,0xE3,0xE7,0xE3,0xE7,0xE1,0xE3,0xE0,
  0xE1,0xE0,0xE0,0xE0,0xC0,0x00,0x7F,0x7F,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x7F,0xFF,0x0F,0x1F,0x00,0x80,0x80,
  0xFF,0xF9,0xFB,0xF9,0xFB,0xF9,0xFB,0xF9,
  0xFB,0xF9,0xF3,0xE1,0xC3,0x00,0x06,0x06,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFD,
  0xFF,0xFC,0xFC,0xFC,0xF8,0x00,0x00,0x00,
  0xFF,0xFF,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,
  0xFC,0x7C,0xF8,0x18,0x30,0x00,0xC0,0xC0,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x01,0x00,0x01,0x01,
  0x03,0x03,0x07,0x03,0x07,0x07,0x0F,0x07,
  0x3F,0x3B,0xFF,0xE3,0xFF,0xCF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xFC,0xFC,
  0xE0,0xE0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,
  0xF0,0xF0,0xE0,0xE0,0xC0,0x00,0x01,0x00,
  0x3F,0x33,0x7F,0x27,0x7F,0x2F,0x7F,0x7F,
  0xFF,0x7F,0xFF,0x7F,0xFF,0xFF,0xFF,0xFC,
  0xF0,0xF0,0xF0,0xF0,0xF8,0xF8,0xF8,0xF8,
  0xF8,0xF8,0xFC,0xFC,0xFC,0xFC,0xFE,0xFE,
  0x3F,0x33,0x7F,0x21,0x7F,0x2F,0x7F,0x3F,
  0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,
  0xE7,0xE3,0xF7,0xF2,0xF7,0xF2,0xFF,0xFB,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFB,0x39,0xFB,0x79,0xFF,0xFD,0xFF,0xFD,
  0xFF,0xFD,0xFF,0xFC,0xFD,0xFC,0xFC,0xFC,
  0xEF,0x6C,0xFF,0x69,0xFF,0xED,0xFF,0xEF,
  0xFF,0xEF,0xFF,0xEF,0xDF,0x03,0x07,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x3F,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFE,0xFE,0xFC,0xC0,
  0x7F,0x67,0xFF,0x4F,0xFF,0x5F,0xFF,0x7F,
  0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFE,0xFE,0xFC,0xC0,
  0xFF,0x4F,0xFF,0x4F,0xFF,0x5F,0xFF,0x7F,
  0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,
  0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
  0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
  0x1F,0x1F,0x3F,0x19,0x3F,0x3C,0x7F,0x7F,
  0xFF,0x7F,0xFF,0x3F,0x7F,0x3F,0x7F,0x1F,
  0x01,0x01,0x83,0x83,0xCF,0xCE,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xE0,0xE0,0xF0,0x30,0xF8,0x78,0xFC,0xFC,
  0xFC,0xFC,0xF8,0xF8,0xF0,0xF0,0xE0,0xE0,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x0F,0x07,0x0F,0x07,0x0F,0x07,0x0F,0x07,
  0x0F,0x07,0x0F,0x03,0x07,0x03,0x07,0x01,
  0xF8,0xF8,0xF8,0xF8,0xF8,0xF8,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x01,0x00,0x01,0x01,0x03,0x01,0xF3,0xF1,
  0xF3,0xF1,0xFB,0xF9,0xFB,0xFB,0xFF,0xFB,
  0xFD,0xFC,0xFC,0xFC,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xF8,
  0xFE,0x7E,0xFE,0x7E,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,
  0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,
  0x7F,0x3F,0x7F,0x3F,0xFF,0xBF,0xFF,0xBF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xDF,0xBF,0x8F,
  0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,
  0xFC,0xFC,0xF8,0xF8,0xF8,0xF8,0xF8,0xF8,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,
  0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,
  0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,
  0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,
  0xFF,0x7F,0xFF,0x7F,0xFF,0x3F,0x7F,0x3F,
  0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,0x7F,0x3F,
  0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,
  0xFE,0xFE,0xFC,0xFC,0xF8,0xF8,0xF0,0xC0,
  0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,
  0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,0xFF,0x7F,
  0x80,0x80,0x80,0x80,0xC4,0xC4,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x3F,0x0F,0x1F,0x07,0x0F,0x03,0x07,0x01,
  0x03,0x01,0x03,0x01,0x03,0x01,0x03,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xE0,0xE0,0xC0,0xC0,0x80,0x80,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x03,0x00,0x01,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xFF,0xFF,0x7F,0xFF,0x0F,0x1F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xFB,0xFF,0xFB,0xF7,0xE3,0xC7,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xF8,0xF8,0xF0,0xF0,0xF0,0xF0,0xE0,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0x3F,0x7F,0x3F,0x7F,0x1F,0x3F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xBF,0xFF,0xDF,0xBF,0x9F,0x3F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x9F,0x87,0x8F,0x83,0x87,0x81,0x03,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xF8,0xF8,0xF8,0xF8,0xF8,0xF8,0xF0,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x3F,0x7F,0x1F,0x3F,0x1F,0x3F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xC0,0xC0,0xC0,0xC0,0x80,0x80,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x3F,0x7F,0x1F,0x3F,0x1F,0x3F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x80,0x80,0x80,0x80,0x80,0x80,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0x3F,0x7F,0x3F,0x7F,0x1F,0x3F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFE,0xFE,0xFC,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xFF,0xFE,0xFE,0xFE,0xFE,0xFC,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x0F,0x0F,0x0F,0x0F,0x07,0x07,
  0xC7,0xC7,0xC7,0xC7,0xE7,0xE7,0xE7,0xE7,
  0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xF1,0xF1,
  0x00,0x00,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,
  0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,
  0x00,0x00,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x00,0x00,0xDF,0xDF,0xDF,0xDF,0xDF,0xDF,
  0xDF,0xDF,0x9F,0x9F,0x8F,0x8F,0x8F,0x8F,
  0x00,0x00,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x0F,0x0F,0x0F,0x0F,0x0F,0x0F,
  0xCF,0xCF,0xCF,0xCF,0xE7,0xE7,0xE7,0xE7
};

/* End of TILES_SPLASH.C */
